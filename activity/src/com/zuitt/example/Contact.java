package com.zuitt.example;

public class Contact {

    private String name;
    private String contactNumber;
    private String address;

    public Contact() {
    }

    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if (!name.isEmpty() && !name.isBlank()) {
            this.name = name;
        } else {
            System.out.println("Please provide your name!");
        }
    }

    public String getContactNumber() {
        return this.contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        if (!contactNumber.isEmpty() && !contactNumber.isBlank()) {
            this.contactNumber = contactNumber;
        } else {
            System.out.println("Please provide a contact number!");
        }
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        if (!address.isEmpty() && !address.isBlank()) {
            this.address = address;
        } else {
            System.out.println("Please provide an address!");
        }
    }
}
