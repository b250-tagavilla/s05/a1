package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook() {
    }

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

    /* Methods */
    public void displayContacts() {
        if (this.contacts.isEmpty()) {
            System.out.println("Phonebook is currently empty.");
        } else {
            System.out.println("Contact List");
            contacts.forEach((contact) -> {
                printInfo(contact);
            });
        }
    }

    public void printInfo(Contact contact) {
        String name = contact.getName();
        String contactNo = contact.getContactNumber();
        String address = contact.getAddress();

        // Contact info will not be displayed if there is a missing data
        if (!name.isEmpty() && !name.isBlank() &&
                !contactNo.isEmpty() && !contactNo.isBlank() &&
                !address.isEmpty() && !address.isBlank()) {
            System.out.println("==============================================");
            System.out.println(name);
            System.out.println("==============================================");
            System.out.println(name + " has the following registered number:");
            System.out.println(contactNo);
            System.out.println(name + " has the following registered address:");
            System.out.println("My home is in " + address + "\n");
        }
    }
}
