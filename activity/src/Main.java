import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City");

        Contact contact2 = new Contact();
        contact2.setName("Jane Doe");
        contact2.setContactNumber("+639162148573");
        contact2.setAddress("Caloocan City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        /* Displays the info of all the contacts in phonebook */
        phonebook.displayContacts();

        /* Displays the info a specific contact */
        phonebook.printInfo(contact1);
        phonebook.printInfo(contact2);
    }
}